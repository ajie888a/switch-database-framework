﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace SwitchDatabaseFramework
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new StudentMgrContext())
            {
                School school = new School();
                school.Name = "张三";
                db.Schools.Add(school);
                int i = db.SaveChanges();

                string temp = i == 1 ? "添加成功" : "添加失败";
                Console.WriteLine(temp);
            }
        }
    }
}
