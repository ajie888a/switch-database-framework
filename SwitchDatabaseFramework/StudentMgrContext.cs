﻿using Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.CodeFirst;

namespace SwitchDatabaseFramework
{
    public class StudentMgrContext:DbContext
    {
        public DbSet<Student> Students { get; set; }

        public DbSet<School> Schools { get; set; }

        public DbSet<SchoolClass> SchoolClasses { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);
            var initer = new SqliteDropCreateDatabaseWhenModelChanges<StudentMgrContext>(modelBuilder);
            Database.SetInitializer(initer);
        }
    }
}
